<?php

declare(strict_types=1);

namespace Honeycombs\Configuration;

use Dotenv\Dotenv;
use Honeycombs\DI\ServiceContainer;

/**
 * Class Configuration
 * @todo .env support
 */
class Configuration
{
    /**
     * .env file path
     *
     * @var string
     */
    private $envPath;

    /**
     * @throws \Honeycombs\Configuration\Exception\ConfigurationException
     * @return \Honeycombs\Configuration\Configuration
     */
    public function init(): self
    {
        $this->prepareConfiguration();

        return $this;
    }

    /**
     * Gets .env file path
     *
     * @return string
     */
    public function getEnvPath(): string
    {
        return $this->envPath;
    }

    /**
     * Sets .env file path
     *
     * @param string $envPath
     * @return Configuration
     */
    public function setEnvPath(string $envPath): self
    {
        $this->envPath = $envPath;

        return $this;
    }

    /**
     * Loads configuration class and apply injects
     *
     * @return void
     */
    private function prepareConfiguration(): void
    {
        if ($this->getEnvPath()) {
            (new Dotenv($this->getEnvPath()))->load();
        }
        $serviceContainer = new ServiceContainer();
        $serviceContainer->resolveInjects($this);
    }
}
