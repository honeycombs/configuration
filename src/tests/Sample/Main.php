<?php

declare(strict_types=1);

namespace Tests\Honeycombs\Configuration\Sample;

use Honeycombs\Configuration\Configuration;

/**
 * Class Main
 *
 * Main configuration
 */
class Main extends Configuration
{
    /**
     * @var Project
     *
     * @inject
     */
    public $project;

    /**
     * @var Database
     *
     * @inject
     */
    public $database;
}
