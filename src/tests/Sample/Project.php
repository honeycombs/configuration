<?php

declare(strict_types=1);

namespace Tests\Honeycombs\Configuration\Sample;

/**
 * Class Project
 *
 * Project configuration
 */
class Project
{
    /**
     * Develop mode
     *
     * @var bool
     */
    public $devMode = false;
}
