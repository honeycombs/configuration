<?php

declare(strict_types=1);

namespace Tests\Honeycombs\Configuration\Sample;

/**
 * Class Main
 *
 * Database configuration
 */
class Database
{
    /**
     * @var string Database host
     */
    public $host = 'localhost';
}
