<?php

declare(strict_types=1);

namespace Tests\Honeycombs\Configuration;

use Honeycombs\Configuration\Configuration;
use PHPUnit\Framework\TestCase;
use Tests\Honeycombs\Configuration\Sample\Database;
use Tests\Honeycombs\Configuration\Sample\Main;

/**
 * Class ConfigurationTest
 *
 * @covers \Honeycombs\Configuration\Configuration
 * @coversDefaultClass  \Honeycombs\Configuration\Configuration
 */
class ConfigurationTest extends TestCase
{
    /**
     * Test setting/getting .env file path
     * @return void
     * @covers ::setConfigurationFilePath
     * @covers ::getConfigurationFilePath
     */
    public function testEnvFilePath(): void
    {
        $configuration = new Configuration();
        $filePath = __DIR__;
        $configuration->setEnvPath($filePath);
        $this->assertEquals($filePath, $configuration->getEnvPath());
    }

    /**
     * Test getting configuration values without init call
     *
     * @return void
     */
    public function testGetWithoutInit(): void
    {
        $configuration = new Main();

        try {
            $configuration->database->host;
        } catch (\Throwable $exception) {
            $exceptionMessage = $exception->getMessage();
        }
        $this->assertEquals('Trying to get property of non-object', $exceptionMessage);
    }

    /**
     * Test getting configuration values
     *
     * @return void
     */
    public function testGet(): void
    {
        $configuration = new Main();
        $configuration->init();
        $this->assertEquals($configuration->database->host, (new Database())->host);
    }
}
