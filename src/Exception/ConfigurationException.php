<?php

declare(strict_types=1);

namespace Honeycombs\Configuration\Exception;

use Exception;

class ConfigurationException extends Exception
{
}
